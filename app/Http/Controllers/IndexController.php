<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function getHome() {
        $mentors = $this->getMentors();
        $phases = $this->getPhases();
        $questions = $this->getQuestions();
        $data = compact('mentors', 'phases', 'questions');
        return view('home', $data);
    }

    public function getMentors() {
        $mentor1 = new \stdClass();
        $mentor1->name = "Nana Baffour";
        $mentor1->position = "CEO";
        $mentor1->social_icon = "website";
        $mentor1->social_link = "";
        $mentor1->image = "img/mentors/nana.jpg";
        $mentor1->description = "Formado em Economia, possui Master, MBA e o título de Chartered Financial Analyst (CFA). Foi presidente e CEO do Grupo Cimcorp e do Grupo Getronics e é fundador da holding de investimentos Bottega S.a.r.l.";

        $mentor2 = new \stdClass();
        $mentor2->name = "Rogério Dias";
        $mentor2->position = "Chief of Staff";
        $mentor2->social_icon = "linkedin";
        $mentor2->social_link = "";
        $mentor2->image = "img/mentors/rogerio.jpg";
        $mentor2->description = "+35 anos de experiência na gestão de equipes com vasto conhecimento do mercado de TI, com atuação nas áreas de Vendas e Operações. Possui sólida experiência em projetos de soluções complexas para ambientes de missões críticas.";

        $mentor3 = new \stdClass();
        $mentor3->name = "Lauro Chacon";
        $mentor3->position = "CHRO";
        $mentor3->social_icon = "linkedin";
        $mentor3->social_link = "";
        $mentor3->image = "img/mentors/lauro.jpg";
        $mentor3->description = "Foi premiado pelo Top Of Mind de RH como um dos 50 profissionais de RH mais admirados no Brasil, atuou como diretor de Recursos Humanos da Accenture Brasil e estrategista de talentos LATAM por 22 anos.";
        
        $mentor4 = new \stdClass();
        $mentor4->name = "Gilberto Caparica";
        $mentor4->position = "COO e CDO LATAM";
        $mentor4->social_icon = "linkedin";
        $mentor4->social_link = "";
        $mentor4->image = "img/mentors/gilberto.jpg";
        $mentor4->description = "Administrador de Empresas com especialização em Gestão Empresarial pela Fundação Dom Cabral, possui +30 anos de experiência na área de Tecnologia, com atuação em várias posições executivas nas áreas de Operações, Serviços, Vendas, Parcerias e Marketing.";

        $mentor5 = new \stdClass();
        $mentor5->name = "Raul Rocha";
        $mentor5->position = "CSO";
        $mentor5->social_icon = "linkedin";
        $mentor5->social_link = "";
        $mentor5->image = "img/mentors/raul.jpg";
        $mentor5->description = "Matemático e Analista de Sistemas com MBA em Gestão Empresarial, Raul possui mais de 26 anos de experiência na área de Tecnologia em empresas de consultoria como Tivit, Sonda, T-Systems e Stefanini.";
        
        $mentor6 = new \stdClass();
        $mentor6->name = "Thais Ducatti";
        $mentor6->position = "Head de Marketing LATAM";
        $mentor6->social_icon = "linkedin";
        $mentor6->social_link = "";
        $mentor6->image = "img/mentors/thais.jpg";
        $mentor6->description = "No setor de TI há +10 anos, é Jornalista com pós-graduação em Comunicação Internacional e Organizacional pela PUC-SP, e especializações em Planejamento Estratégico de Marketing, Comunicação Integrada e Gestão de Marcas pela ESPM.";

        $mentor7 = new \stdClass();
        $mentor7->name = "Breno Barros";
        $mentor7->position = "Chief Digital & Innovation Office ";
        $mentor7->social_icon = "linkedin";
        $mentor7->social_link = "";
        $mentor7->image = "img/mentors/breno.jpg";
        $mentor7->description = "Graduado em Ciência da Computação e Marketing, possui Pós-Graduação em Mídia Digital e Especialização em Transformação Digital pela INSEAD/França, além de Pós-MBA em Futuro & Tendência. É professor de MBA em Digital Business na FIAP e Mentor/Instrutor na TurnVision.";

        return [$mentor1, $mentor2, $mentor3, $mentor4, $mentor5, $mentor6, $mentor7];
    }

    public function getPhases() {
        $phase1 = new \stdClass();
        $phase1->dates = "De 22/06/2020 a 10/07/2020";
        $phase1->name = "Inscrição";
        $phase1->description = "Preencha o formulário de inscrição para que a sua Startup seja avaliada.";

        $phase2 = new \stdClass();
        $phase2->dates = "De 23/06/2020 a 17/07/2020";
        $phase2->name = "Avaliação e entrevistas";
        $phase2->description = "A Qintess entrevistará as Startups com maior 
        sinergia através de conference calls.";

        $phase3 = new \stdClass();
        $phase3->dates = "De 23/07/2020 a 24/07/2020";
        $phase3->name = "Pitch Day";
        $phase3->description = "Avaliação dos pitches selecionados pela Qintess. (Presencial ou não, dependerá da situação do COVID-19)";

        $phase4 = new \stdClass();
        $phase4->dates = "De 27/07/2020";
        $phase4->name = "Divulgação dos selecionados";
        $phase4->description = "Após liberação interna do resultado, a Qintess comunicará as Startups selecionadas e divulgará na mídia.";
        
        $phase5 = new \stdClass();
        $phase5->dates = "De 28/07/2020 a 28/07/2021";
        $phase5->name = "Início do programa";
        $phase5->description = "12 meses de aceleração com a Qintess, que inclui onboarding, estratégia go to market, planejamento de contas, geração de demandas, shadow commercial, apoio no relacionamento e monitoramento de KPIs.";

        return [$phase1, $phase2, $phase3, $phase4, $phase5];
    }

    public function getQuestions() {
        $q1 = new \stdClass();
        $q1->title = "O que é o Qintess Ignite Startups?";
        $q1->answer = "É um programa de aceleração de startups que tem, como objetivo, gerar parcerias de negócio e alavancar o crescimento de startups em nossas base de clientes, com um força de vendas forte, presença global, suporte a desenvolvimento e evolução de seus produtos e com sessões de mentorias.";

        $q2 = new \stdClass();
        $q2->title = "Quais as contrapartidas exigidas do programa? Vocês compram equity das Startups? Como fica a propriedade intelectual da minha solução?";
        $q2->answer = "A principal contrapartida exigida é o comprometimento do time e a vontade de gerar negócios. A Qintess não compra Equity num primeiro momento, porém,  pede para ter um “right of first offer”. O nosso principal objetivo é gerar negócio e alavancar seu crescimento. Não temos interesse em nos apropriarmos da propriedade intelectual da sua startup. Quais as contrapartidas exigidas do programa? Vocês compram equity das Startups? Como fica a propriedade intelectual da minha solução?";

        $q3 = new \stdClass();
        $q3->title = "Tenho uma ideia ainda não desenvolvida. Posso participar da seleção?";
        $q3->answer = "Não. Este programa é feito para startups que comprovaram uma certa aderência no mercado ou já têm clientes faturados.";
        
        $q4 = new \stdClass();
        $q4->title = "Eu preciso estar em São Paulo para participar do programa?";
        $q4->answer = "Não é obrigatório, porém recomendamos a estadia em São Paulo durante o programa de aceleração. ";
       
        $q5 = new \stdClass();
        $q5->title = "Existe alguma ajuda de custo para as Startups selecionadas fora de São Paulo?";
        $q5->answer = "Não. A startup deve arcar com os custos de deslocamento e estadia durante o programa. Se a startup estiver localizada fora de São Paulo, as sessões de mentorias poderão ser feitas por videoconferência.";
       
        $q6 = new \stdClass();
        $q6->title = "Vocês vão assinar um acordo de confidencialidade? Como sei que não vão roubar a minha ideia?";
        $q6->answer = "Nós não assinamos acordo de confidencialidade no momento do processo de seleção, mas pode ficar tranquilo. Nós trataremos com sigilo todas as informações compartilhadas. Apenas a equipe da Qintess terá acesso aos dados. Um termo de uso será assinado se sua startup for selecionada, incluindo um termo de confidencialidade.";

        $q7 = new \stdClass();
        $q7->title = "Posso fazer a inscrição como pessoa física ou preciso ter um CNPJ?";
        $q7->answer = "Sim. É obrigatório ter um CNJP!";

        $q8 = new \stdClass();
        $q8->title = "Caso eu não participe do Pitch Day, ainda posso participar do programa com a minha equipe?";
        $q8->answer = "Não. Porque é importante participar do Pitchday para responder as perguntas. A performance da apresentação e da equipe é um dos critérios de seleção.";

        $q9 = new \stdClass();
        $q9->title = "Caso minha Startup não seja selecionada, eu posso me inscrever de novo na próxima edição?";
        $q9->answer = "Sim, sem dúvidas!";

        $q10 = new \stdClass();
        $q10->title = "Se não for selecionada, a Qintess ficará com os meus dados se tiver outras oportunidades?";
        $q10->answer = "Sim, sem dúvida! Se você permitir.";

        return [$q1, $q2, $q3, $q4, $q5, $q6, $q7, $q8, $q9, $q10];
    }


}
