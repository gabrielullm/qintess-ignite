<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Qintess Ignite Startups</title>
        <link href="css/app.css" rel="stylesheet">
    </head>

    <body>
        <section id="main">
            <div class="container">
                <div class="row header">
                    <div class="col-1"></div>
                    <div class="col-8 logo">
                        <img id="logo-qintess" src="img\logos\logo-qintess.svg" alt="Logo Qintess">
                    </div>
                    <div class="col-3 social">
                        <a href="https://www.instagram.com/qintessoficial/" target="_blank">
                            <img class="social-ig" src="img\social\ig.svg" alt="Instagram">
                        </a>
                        <a href="https://www.linkedin.com/company/qintess/" target="_blank">
                            <img class="social-lk-white" src="img\social\lk-white.svg" alt="LinkedIn">
                        </a>
                        <a href="https://www.youtube.com/user/ResourceITsolutions" target="_blank">
                            <img class="social-yt" src="img\social\yt.svg" alt="YouTube">
                        </a>
                        <a href="https://open.spotify.com/show/5RZYbjSE9pCv42sx7aH11V" target="_blank">
                            <img class="social-sp" src="img\social\sp.svg" alt="Spotify">
                        </a>
                    </div>
                </div>
                <div class="row logo">
                    <div class="col-1"></div>
                    <div class="col-11 ignite">
                        <img id="logo-ignite" src="img\logos\logo-ignite.svg" alt="Qintess Ignite Startups">
                    </div>
                </div>
                <div class="row description">
                    <div class="col-1"></div>
                    <div class="col-3">
                        Um programa de aceleração que vai gerar novos negócios inovadores e alavancar o crescimento da sua startup em nosso ecossistema de clientes, shareholders e parceiros
                    </div>
                </div>
                <div class="row button">
                    <div class="col-1"></div>
                    <div class="col-11">
                        <button class="row button-sub gradient" title="Faça sua inscrição">
                            <a href="#inscricao">FAÇA SUA INSCRIÇÃO</a>
                        </button>
                    </div>
                </div>

                <div class="bg-elements">
                    <div class="arc1"></div>
                    <div class="circle1"></div>
                    <div class="circle2"></div>
                    <div class="circle3"></div>
                    <div class="circle4"></div>
                    <div class="circle5"></div>
                    <div class="circle6"></div>
                    <div class="circle7"></div>
                    <div class="circle8"></div>
                    <div class="circle9"></div>
                    <div class="saturn"></div>
                    <div class="rocket"></div>
                </div>
            </div>
        </section>

        <section id="offer">
            <div class="container">
                <div class="row subtitle">
                    <div class="col-12">
                        <h2>O que o Qintess Ignite Startups oferece?</h2>
                    </div>
                </div>
                <div class="row texts">
                    <div class="col-1"></div>
                    <div class="col-5">
                        <p class="highlight">Parceria em Negócios</p>
                        <p>O nosso objetivo é promover uma parceria estratégica de negócio – sem equity no primeiro momento - com a sua startup, podendo ser uma parceria de aceleração de distribuição comercial, complementação de portfólio ou criação de um novo produto.</p>
                    </div>
                    <div class="col-1"></div>
                    <div class="col-5">
                        <p class="highlight">Coworking em Nosso Habitat </p>
                        <p>Durante o programa,  ofereceremos para as startups um espaço físico altamente moderno, high tech e colaborativo dentro do nosso novo escritório, localizado na Rua Bela Cintra, na cidade de São Paulo. Você terá acesso a todo o nosso ecossistema de inovação global!</p>
                    </div>
                </div>
                <div class="row texts">
                    <div class="col-1"></div>
                    <div class="col-5">
                        <p class="highlight">Acesso ao Nosso Crowdsourcing</p>
                        <p>Durante o programa, também iremos disponibilizar mensalmente algumas horas de nossos talentos técnicos. Hoje, contamos com mais de 3.000 colaboradores, e eles estão dispostos a contribuir com a evolução do produto de sua startup.</p>
                    </div>
                    <div class="col-1"></div>
                    <div class="col-5">
                        <p class="highlight">Mentoria e Coaching </p>
                        <p>Serão promovidas sessões com as startups selecionadas, por meio de  um cronograma mensal de palestras, talks e mentorias com nossas equipes e executivos (que possuem vasta experiência e visão global), além de mentores de mercado e coaching com acompanhamento.</p>
                    </div>
                </div>
                <div class="row texts">
                    <div class="col-1"></div>
                    <div class="col-5">
                        <p class="highlight">Investimento Financeiro</p>
                        <p>Vamos avaliar continuamente a tração dos resultados de negócios conjuntos, durante o período do programa. Mesmo que, em um primeiro momento, não tenhamos o objetivo de equity, iremos avaliar a possibilidade de investir financeiramente em seu negócio, caso seja de interesse de ambos os lados.</p>
                    </div>
                    <div class="col-1"></div>
                    <div class="col-5">
                        <img src="img\bg\teamwork.svg" alt="Pessoas trabalhando juntas">
                    </div>
                </div>
                <div class="row button">
                    <div class="col-1"></div>
                    <div class="col-11">
                        <button class="row button-sub gradient" title="Faça sua inscrição">
                            FAÇA SUA INSCRIÇÃO
                        </button>
                    </div>
                </div>
            </div>
        </section>

        <section id="mentors">
            <div class="container">

                <div class="row subtitle">
                    <div class="col-12">
                        <h2>Quem serão os mentores internos?</h2>
                    </div>
                </div>

                <div class="row justify-content-center mentors">
                    @foreach($mentors as $mentor)
                        <div class="col-3">
                                <img src="{{ $mentor->image }}" alt="Foto de {{ $mentor->name }}">
                                <p class="name">{{ $mentor->name }}</p>
                                <a class="icon-wrapper" href="{{ $mentor->social_link }}" target="_blank">
                                    <span class="icon {{ $mentor->social_icon }}"></span>
                                    <p class="position">{{ $mentor->position }}</p>
                                </a>
                                <span class="clear"></span>
                                <p class="description">{{ $mentor->description }}</p>
                        </div>
                    @endforeach
                </div>
                <div class="row justify-content-center button">
                    <div class="col-1"></div>
                    <div class="col-3">
                        <button class="row button-sub gradient" title="Faça sua inscrição">
                            FAÇA SUA INSCRIÇÃO
                        </button>
                    </div>
                </div>
            </div>
        </section>

        <section id="reqs">
            <div class="container">
                <div class="row subtitle">
                    <div class="col-12">
                        <h2>Conheça os requisitos para se inscrever</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-4">
                        <div class="req">
                            <div class="num">01</div>
                            <div class="text">
                                As Startups devem ter obtido no mínimo R$ 500.000,00 de faturamento no ano de 2019. Ou seja, já ter clientes faturados.
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="req">
                            <div class="num">03</div>
                            <div class="text">
                                Ter founders que estejam extremamente dedicados ao processo de aceleração.
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="req">
                            <div class="num">05</div>
                            <div class="text">
                                Se comprometer com a rotina de reuniões e mentorias com a Qintess.
                            </div>   
                            <div class="clear"></div>
                        </div>                       
                    </div> 
                    <div class="col-1"></div>       
                    <div class="col-4">
                        <div class="req">
                            <div class="num">02</div>
                            <div class="text">
                                Não deve ter parcerias com outras empresas de serviços de tecnologia. Porém, estar participando de outros processos de aceleração, não exclui a chance de participar do nosso processo.
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="req">
                            <div class="num">04</div>
                            <div class="text">
                                Ter disponibilidade para estar em São Paulo durante o período de aceleração.
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="req">
                            <div class="num">06</div>
                            <div class="text">
                                Colaborar no desenvolvimento de ofertas de novas soluções, complementações de ofertas atuais que enderecem desafios e/ ou problemas de negócios de nossos clientes atuais e futuros.
                            </div>
                            <div class="clear"></div>
                        </div>        
                    </div>
                    <div class="col-1"></div>
                </div>
                <div class="row themes">
                    <div class="col-12">
                        <div class="row subtitle">
                            <div class="col-12">
                                <h3>As Startups devem inscrever suas soluções respeitando os segmentos / temas abaixo:  </h3>
                            </div>
                        </div>
                        <div class="row solutions">
                            <div class="col-2"></div>
                            <div class="col-3 left">
                                <p class="selected">Digital Finance & Financial Retail</p>
                                <hr>
                                <p>Digital Insurance </p>
                                <hr>
                                <p>Digital Retail & CPG </p>
                                <hr>
                                <p>Digital Government </p>
                                <hr>
                            </div>
                            <div class="col-1"></div>
                            <div class="col-4 right">
                                <ul>
                                    <li>Plataforma de Banco Digital ou Wallets</li>
                                    <li>Monetização de APIs visando Open Banking</li>
                                    <li>Pagamento Instantâneo</li>
                                    <li>Automações de Processos (OCRs, IA, etc)</li>
                                    <li>Análise de Risco e de Redução de Fraudes</li>
                                    <li>Loyalties</li>
                                    <li>Recuperação de Dívidas</li>
                                    <li>Atendimento Omni-Channel </li>
                                    <li>Marketing Digital e Media Performance para aquisição de novos clientes</li>
                                </ul>
                            </div>
                            <div class="col-2">
                                <div class="arrow-solutions"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center button">
                    <div class="col-2">
                        <button class="row button-sub gradient" title="Faça sua inscrição">
                            <a href="#inscricao">FAÇA SUA INSCRIÇÃO</a>
                        </button>
                    </div>
                </div>
            </div>
        </section>

        <section id="how">
            <div class="container">
                <div class="row subtitle">
                    <div class="col-12">
                        <h2>Como vai funcionar?</h2>
                    </div>
                </div>
                @foreach($phases as $phase)
                    <div class="row item">
                        <div class="col-1"></div>
                        <div class="col-3 phase">
                            <p class="date">{{$phase->dates}}</p>
                            <p class="text">{{$phase->name}}</p>
                        </div>
                        <div class="col-2 num">
                            <div class="circle {{$loop->last ? 'last' : 'regular'}}">
                                <p>{{$loop->iteration}}</p>
                            </div>
                        </div>
                        <div class="col-5 description">
                            <p>{{$phase->description}}</p>
                        </div>
                        <div class="col-1"></div>
                    </div>
                @endforeach
                <div class="separator"></div>
                <div class="row justify-content-center button">
                    <div class="col-2">
                        <button class="row button-sub red" title="Faça sua inscrição">
                            <a href="#inscricao">FAÇA SUA INSCRIÇÃO</a>
                        </button>
                    </div>
                </div>
            </div>
        </section>

        <section id="faq">
            <div class="container">
                <div class="row subtitle">
                    <div class="col-12">
                        <h2>Perguntas frequentes</h2>
                        <h3>FAQ</h3>
                    </div>
                </div>
                @foreach($questions as $question)
                    <div class="row items">
                        <div class="col-1"></div>
                        <div class="col-10 item">
                            <span class="icon">+</span>
                            <p class="question">{{$question->title}}</p>
                            <p class="answer collapsed">{{$question->answer}}</p>
                            <hr>
                        </div>
                        <div class="col-1"></div>
                    </div>
                @endforeach
                <div class="separator"></div>
                <div class="row justify-content-center button">
                    <div class="col-2">
                        <button class="row button-sub red" title="Faça sua inscrição">
                            <a href="#inscricao">FAÇA SUA INSCRIÇÃO</a>
                        </button>
                    </div>
                </div>
            </div>
        </section>

        <section id="subs">
            <div class="container">
                <div class="row subtitle">
                    <div id="inscricao" class="col-12">
                        <h2>Formulário de inscrição</h2> 
                    </div>      
                </div>
                <div class="row justify-content-center fields">
                    <form action="#">
                        <div class="field">
                            <label for="name">Nome da Startup</label>
                            <input id="name" name="name" type="text" maxlength="240">
                        </div>
                        <div class="field">
                            <label for="proposition">Proposição de Valor</label>
                            <textarea id="proposition" name="proposition" maxlength="5000"></textarea>
                        </div>
                        <div class="field">
                            <label for="segment">Segmento da solução</label>
                            <select name="segment" id="segment">
                                <option value="0">Selecione...</option>
                            </select>
                        </div>
                        <div class="field">
                            <label for="site">Site da Startup</label>
                            <input id="site" name="site" type="text" maxlength="80">
                        </div>
                        <div class="field">
                            <label for="email">E-mail de contato</label>
                            <input id="email" name="email" type="email" maxlength="80">
                        </div>
                        <div class="field">
                            <label for="phone">Telefone/WhatsApp de contato</label>
                            <input id="phone" name="phone" type="text" maxlength="20">
                        </div>
                        <div class="field">
                            <label for="revenue">Faturamento anual aproximado</label>
                            <input id="revenue" name="revenue" type="number" min="0">
                        </div>
                        <div class="field">
                            <label for="linkedin">LinkedIn da Startup</label>
                            <input id="linkedin" name="linkedin" type="text" maxlength="480">
                        </div>
                        <div class="row justify-content-center button">
                            <div class="col-6">
                                <button class="row button-sub gradient" title="Faça sua inscrição">
                                    <a href="#inscricao">FAÇA SUA INSCRIÇÃO</a>
                                </button>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </section>

        <section id="about">
            <div class="container">
                <div class="row subtitle">
                    <div class="col-12">
                        <h2>Sobre a Qintess</h2> 
                    </div>      
                </div>
                <div class="row logo">
                    <div class="col-12">
                        <img id="logo-qintess" src="img\logos\logo-qintess.svg" alt="Logo Qintess">
                    </div>
                </div>
                <div class="row justify-content-center text">
                    <div class="col-10">
                        <p>
                        A Qintess combinou a experiência em estratégia, design de inovação e tecnologia para se tornar líder em transformação digital, desenvolvendo recursos digitais e habilitando a TI para apoiar a jornada de seus clientes em direção ao crescimento sustentável dos negócios.
                        </p>
                        <p>
                        Com uma obsessão por inovação e agilidade na tomada de decisões, a Qintess acelera a entrega de soluções para os diferentes desafios de negócios, gerando valor real para o ecossistema de clientes e parceiros. Todo esse trabalho está alinhado com as práticas ESG (Environmental, Social and Corporate Governance, na sigla em inglês), e a uma preocupação genuína com os nossos colaboradores e as comunidades nas quais operamos.
                        </p>
                        <p>
                        Com cerca de 3.500 funcionários e operações no Brasil e no Chile. Os principais clientes da Qintess incluem sete das dez principais instituições financeiras do mundo, oito das dez maiores empresas de serviços públicos do Brasil, além de duas das três maiores empresas de telecomunicações do país.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-5 name-red">
                        <p>quintess.com</p>
                        <img src="img\bg\motto.svg" alt="Innovation obsessed">
                    </div>
                    <div class="col-3"></div>
                    <div class="col-2 social">
                        <a href="https://www.instagram.com/qintessoficial/" target="_blank">
                            <img class="social-ig" src="img\social\ig.svg" alt="Instagram">
                        </a>
                        <a href="https://www.linkedin.com/company/qintess/" target="_blank">
                            <img class="social-lk-white" src="img\social\lk-white.svg" alt="LinkedIn">
                        </a>
                        <a href="https://www.youtube.com/user/ResourceITsolutions" target="_blank">
                            <img class="social-yt" src="img\social\yt.svg" alt="YouTube">
                        </a>
                        <a href="https://open.spotify.com/show/5RZYbjSE9pCv42sx7aH11V" target="_blank">
                            <img class="social-sp" src="img\social\sp.svg" alt="Spotify">
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </body>


</html>
